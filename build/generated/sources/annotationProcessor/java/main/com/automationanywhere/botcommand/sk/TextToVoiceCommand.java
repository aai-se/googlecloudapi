package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class TextToVoiceCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(TextToVoiceCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    TextToVoice command = new TextToVoice();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(!(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("text") && parameters.get("text") != null && parameters.get("text").get() != null) {
      convertedParameters.put("text", parameters.get("text").get());
      if(!(convertedParameters.get("text") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","text", "String", parameters.get("text").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("text") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","text"));
    }

    if(parameters.containsKey("languageCode") && parameters.get("languageCode") != null && parameters.get("languageCode").get() != null) {
      convertedParameters.put("languageCode", parameters.get("languageCode").get());
      if(!(convertedParameters.get("languageCode") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","languageCode", "String", parameters.get("languageCode").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("languageCode") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","languageCode"));
    }

    if(parameters.containsKey("voice") && parameters.get("voice") != null && parameters.get("voice").get() != null) {
      convertedParameters.put("voice", parameters.get("voice").get());
      if(!(convertedParameters.get("voice") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","voice", "String", parameters.get("voice").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("voice") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","voice"));
    }
    if(convertedParameters.get("voice") != null) {
      switch((String)convertedParameters.get("voice")) {
        case "female" : {

        } break;
        case "male" : {

        } break;
        case "neutral" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","voice"));
      }
    }

    if(parameters.containsKey("outputFile") && parameters.get("outputFile") != null && parameters.get("outputFile").get() != null) {
      convertedParameters.put("outputFile", parameters.get("outputFile").get());
      if(!(convertedParameters.get("outputFile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","outputFile", "String", parameters.get("outputFile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("outputFile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","outputFile"));
    }

    if(parameters.containsKey("ssml") && parameters.get("ssml") != null && parameters.get("ssml").get() != null) {
      convertedParameters.put("ssml", parameters.get("ssml").get());
      if(!(convertedParameters.get("ssml") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","ssml", "Boolean", parameters.get("ssml").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("play") && parameters.get("play") != null && parameters.get("play").get() != null) {
      convertedParameters.put("play", parameters.get("play").get());
      if(!(convertedParameters.get("play") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","play", "Boolean", parameters.get("play").get().getClass().getSimpleName()));
      }
    }

    command.setSessions(sessionMap);
    try {
      command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("text"),(String)convertedParameters.get("languageCode"),(String)convertedParameters.get("voice"),(String)convertedParameters.get("outputFile"),(Boolean)convertedParameters.get("ssml"),(Boolean)convertedParameters.get("play"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
